

from deck import Deck
from table import Table

import combos


class FriendsDealer:
    def __init__(self, table: Table):
        self.deck = Deck()
        self.table = table

    def start_game(self):
        assert self.table.all_players_have_joined()
        self.deck.generate_deck()
        self.deck.shuffle()
        self.__deal_to_table()

    def __deal_to_table(self):
        start_card = self.deck.cut()
        index = start_card.number.value % combos.FRIENDS_PLAYER_LIMIT

        while self.deck.stack:
            self.table.deal_to_player(index, self.deck.deal())
            index = (index + 1) % combos.FRIENDS_PLAYER_LIMIT


if __name__ == '__main__':
    from player import FriendsPlayer


    friends_table = Table(combos.FRIENDS_PLAYER_LIMIT)
    for _ in range(combos.FRIENDS_PLAYER_LIMIT):
        friends_table.join(FriendsPlayer())

    dealer = FriendsDealer(friends_table)
    dealer.start_game()
    pass

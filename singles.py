
from cards import Card
from hands import Hand, HandType


class SingleClassifier:
    def calculate_value(self, hand: Hand) -> int:
        if not self.hand_is_correct_len_and_type(hand):
            return -1000

        card = hand.hand[0]
        assert isinstance(card, Card)
        return card.number.value * 10 + card.suit.value

    @staticmethod
    def hand_is_correct_len_and_type(hand: Hand) -> bool:
        return len(hand) is 1 and hand.type is HandType.Singles

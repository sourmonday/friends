
from cards import Card
from hands import Hand, HandType


class TripleClassifier:
    @staticmethod
    def hand_is_three(hand: Hand) -> bool:
        return len(hand) is 3 and hand.type is HandType.Triples

    @staticmethod
    def hand_is_triple(hand: Hand) -> bool:
        first_card = hand.hand[0]
        assert isinstance(first_card, Card)

        for _, c, in enumerate(hand.hand):
            assert isinstance(c, Card)
            if first_card.number.value != c.number.value:
                return False
        return True

    @staticmethod
    def duplicates_exist(hand: Hand) -> bool:
        return len(hand) != len(set(hand.hand))

    def calculate_value(self, hand: Hand) -> int:
        if not self.hand_is_three(hand) or not self.hand_is_triple(hand) or self.duplicates_exist(hand):
            return -1000

        first_card = hand.hand[0]
        assert isinstance(first_card, Card)

        return first_card.number.value


if __name__ == '__main__':
    pass


from cards import Card
from hands import Hand, HandType
from player import PlayerController

import singles
import pairs
import triples
import quads
import combos

from abc import abstractmethod
from enum import Enum


class StackRules(Enum):
    Singles = 1
    Doubles = 2
    Triples = 3
    Quads = 4
    Combos = 5


class TableStack(list):
    def __init__(self):
        super().__init__()
        self.stack_level = 0
        self.rule_checker = None

    def reset_stack_level(self):
        self.stack_level = 0

    def peek(self) -> Hand:
        return self[-1]

    @abstractmethod
    def push(self, hand: Hand):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def __check_handtype(hand: Hand) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __calculate_hand_level(self, hand: Hand) -> int:
        raise NotImplementedError


class SingleStack(TableStack):
    def __init__(self):
        super().__init__()
        self.rule_checker = singles.SingleClassifier()

    def push(self, hand: Hand):
        if not self.__check_handtype(hand):
            raise TypeError

        if self and self.stack_level > 0 and not self.uno_rules_met(hand):
            raise ValueError

        new_stack_level = self.__calculate_hand_level(hand)
        if new_stack_level < self.stack_level:
            raise ValueError

        self.stack_level = new_stack_level
        super().append(hand)

    def uno_rules_met(self, hand: Hand) -> bool:
        top_of_stack = self.peek()
        assert isinstance(top_of_stack, Hand)
        stack_card, c = top_of_stack.hand[0], hand.hand[0]
        assert isinstance(stack_card, Card)
        assert isinstance(c, Card)

        return stack_card.number == c.number or stack_card.suit == c.suit

    @staticmethod
    def __check_handtype(hand: Hand) -> bool:
        return hand.type is HandType.Singles

    def __calculate_hand_level(self, hand: Hand) -> int:
        return self.rule_checker.calculate_value(hand)


class DoubleStack(TableStack):
    def __init__(self):
        super().__init__()
        self.rule_checker = pairs.DoubleClassifier()

    def push(self, hand: Hand):
        if not self.__check_handtype(hand):
            raise TypeError

        new_stack_level = self.__calculate_hand_level(hand)
        if new_stack_level < self.stack_level:
            raise ValueError

        self.stack_level = new_stack_level
        super().append(hand)

    @staticmethod
    def __check_handtype(hand: Hand) -> bool:
        return hand.type is HandType.Doubles

    def __calculate_hand_level(self, hand: Hand) -> int:
        return self.rule_checker.calculate_value(hand)


class TripleStack(TableStack):
    def __init__(self):
        super().__init__()
        self.rule_checker = triples.TripleClassifier()

    def push(self, hand: Hand):
        if not self.__check_handtype(hand):
            raise TypeError

        new_stack_level = self.__calculate_hand_level(hand)
        if new_stack_level < self.stack_level:
            raise ValueError

        self.stack_level = new_stack_level
        super().append(hand)

    @staticmethod
    def __check_handtype(hand: Hand) -> bool:
        return hand.type is HandType.Triples

    def __calculate_hand_level(self, hand: Hand) -> int:
        return self.rule_checker.calculate_value(hand)


class QuadStack(TableStack):
    def __init__(self):
        super().__init__()
        self.rule_checker = quads.QuadClassifier()

    def push(self, hand: Hand):
        if not self.__check_handtype(hand):
            raise TypeError

        new_stack_level = self.__calculate_hand_level(hand)
        if new_stack_level < self.stack_level:
            raise ValueError

        self.stack_level = new_stack_level
        super().append(hand)

    @staticmethod
    def __check_handtype(hand: Hand) -> bool:
        return hand.type is HandType.Quads

    def __calculate_hand_level(self, hand: Hand) -> int:
        return self.rule_checker.calculate_value(hand)



class ComboStack(TableStack):
    def __init__(self):
        super().__init__()
        self.rule_checker = combos.ComboClassifier()

    def push(self, hand: Hand):
        if not self.__check_handtype(hand):
            raise TypeError

        new_stack_level = self.__calculate_hand_level(hand)
        if new_stack_level < self.stack_level:
            raise ValueError

        self.stack_level = new_stack_level
        super().append(hand)

    @staticmethod
    def __check_handtype(hand: Hand) -> bool:
        return hand.type is HandType.Combos

    def __calculate_hand_level(self, hand: Hand) -> int:
        return self.rule_checker.calculate_value(hand)


class Table:
    def __init__(self, player_limit: int):
        self.player_limit = player_limit
        self.players = list()

    def join(self, player: PlayerController) -> None:
        self.players.append(player)
        assert len(self.players) <= self.player_limit

    def all_players_have_joined(self) -> bool:
        return len(self.players) == self.player_limit

    def deal_to_player(self, index, card: Card) -> None:
        player = self.players[index]
        assert isinstance(player, PlayerController)
        player.add_card(card)


table_stack_dispatch = {
    StackRules.Singles: SingleStack,
    StackRules.Doubles: DoubleStack,
    StackRules.Triples: TripleStack,
    StackRules.Quads: QuadStack,
    StackRules.Combos: ComboStack,
}


if __name__ == '__main__':
    pass

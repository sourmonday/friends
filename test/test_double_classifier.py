
import unittest

import pairs

from test.doubles.constant_hands import *


class TestPairs(unittest.TestCase):
    def setUp(self):
        self.classifier = pairs.DoubleClassifier()

    def test_invalid_lengths_fail(self):
        self.assertFalse(self.classifier.hand_is_correct_len_and_type(FULL_HOUSE))
        self.assertGreater(0, self.classifier.calculate_value(FULL_HOUSE))

    def test_invalid_hand_elements(self):
        self.assertFalse(self.classifier.valid_hand_elements(NOT_PAIR))
        self.assertGreater(0, self.classifier.calculate_value(NOT_PAIR))

    def test_dupe_values_fails(self):
        hand = PAIR.copy()
        list_hand = list(hand.hand)

        list_hand[0] = list_hand[1]
        hand.hand = tuple(list_hand)

        self.assertGreater(0, self.classifier.calculate_value(hand))

    def test_values_calculated_correctly(self):
        smaller = self.classifier.calculate_value(PAIR)
        larger = self.classifier.calculate_value(SAME_NUMBER_PAIR)
        different_number = self.classifier.calculate_value(BIGGER_PAIR)

        self.assertGreater(larger, smaller)
        self.assertGreater(different_number, larger)


if __name__ == '__main__':
    unittest.main()

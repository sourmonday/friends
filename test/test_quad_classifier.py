
import unittest

import quads

from test.doubles.constant_hands import *


class TestQuads(unittest.TestCase):
    def setUp(self):
        self.classifier = quads.QuadClassifier()

    def test_invalid_lengths_fail(self):
        self.assertFalse(self.classifier.hand_is_four(FULL_HOUSE))
        self.assertGreater(0, self.classifier.calculate_value(FULL_HOUSE))

    def test_not_a_quad_fail(self):
        self.assertFalse(self.classifier.hand_is_quad(NOT_QUAD))
        self.assertGreater(0, self.classifier.calculate_value(NOT_QUAD))

    def test_values_calculated_correctly(self):
        smaller = self.classifier.calculate_value(SMALLER_QUAD)
        larger = self.classifier.calculate_value(LARGER_QUAD)

        self.assertGreater(larger, smaller)

    def test_dupe_values_fails(self):
        hand = SMALLER_QUAD.copy()
        list_hand = list(hand.hand)

        list_hand[0] = list_hand[1]
        hand.hand = tuple(list_hand)

        self.assertGreater(0, self.classifier.calculate_value(hand))


if __name__ == '__main__':
    unittest.main()


import unittest

import combos

from test.doubles.constant_hands import *


class TestFlush(unittest.TestCase):
    def setUp(self):
        self.classifier = combos.ComboClassifier()

    def test_is_flush_true(self):
        self.assertTrue(self.classifier.is_flush(SPADE_FLUSH))

    def test_is_flush_false(self):
        self.assertFalse(self.classifier.is_flush(STRAIGHT))

    def test_is_flush_too_many(self):
        flush_hand = SPADE_FLUSH.copy()
        new_flush_hand = list(flush_hand.hand)
        new_flush_hand.append(Card(Number.Queen, Suit.Spade))

        flush_hand.hand = tuple(new_flush_hand)
        self.assertFalse(self.classifier.is_flush(flush_hand))

    def test_flush_with_duplicates_fails(self):
        flush_hand = SPADE_FLUSH.copy()
        new_flush_hand = list(flush_hand.hand)
        new_flush_hand[3] = new_flush_hand[4]

        flush_hand.hand = tuple(new_flush_hand)
        self.assertFalse(self.classifier.is_flush(flush_hand))

    def test_range_valid(self):
        hand_level = self.classifier.calculate_value(SPADE_FLUSH)

        self.assertLessEqual(combos.ComboLevels.Flush.value, hand_level)
        self.assertLessEqual(hand_level, combos.ComboLevels.Flush.value + combos.ComboLevels.LevelDelta.value)


class TestStraight(unittest.TestCase):
    def setUp(self):
        self.classifier = combos.ComboClassifier()

    def test_is_straight_true(self):
        self.assertTrue(self.classifier.is_straight(STRAIGHT))

    def test_unordered_true(self):
        self.assertTrue(self.classifier.is_straight(UNORDERED_STRAIGHT))

    def test_is_straight_false(self):
        self.assertFalse(self.classifier.is_straight(SPADE_FLUSH))

    def test_is_straight_too_many(self):
        straight_hand = STRAIGHT.copy()
        new_straight_hand = list(straight_hand.hand)
        new_straight_hand.append(Card(Number.Nine, Suit.Spade))

        straight_hand.hand = tuple(new_straight_hand)
        self.assertFalse(self.classifier.is_straight(straight_hand))

    def test_wrap_is_not_valid(self):
        self.assertFalse(self.classifier.is_straight(INVALID_WRAPPED_STRAIGHT))

    def test_range_valid(self):
        hand_level = self.classifier.calculate_value(STRAIGHT)

        self.assertLessEqual(combos.ComboLevels.Straight.value, hand_level)
        self.assertLessEqual(hand_level, combos.ComboLevels.Straight.value + combos.ComboLevels.LevelDelta.value)


class TestStraightFlush(unittest.TestCase):
    def setUp(self):
        self.classifier = combos.ComboClassifier()

    def test_just_straight(self):
        self.assertFalse(self.classifier.is_straight_flush(STRAIGHT))

    def test_just_flush(self):
        self.assertFalse(self.classifier.is_straight_flush(SPADE_FLUSH))

    def test_straight_flush_true(self):
        self.assertTrue(self.classifier.is_straight_flush(STRAIGHT_FLUSH))

    def test_range_valid(self):
        hand_level = self.classifier.calculate_value(STRAIGHT_FLUSH)

        self.assertLessEqual(combos.ComboLevels.StraightFlush.value, hand_level)
        self.assertLessEqual(hand_level, combos.ComboLevels.StraightFlush.value + combos.ComboLevels.LevelDelta.value)


class TestFullHouse(unittest.TestCase):
    def setUp(self):
        self.classifier = combos.ComboClassifier()

    def test_hand_is_right_length(self):
        hand = FULL_HOUSE.copy()
        new_hand = list(hand.hand)
        new_hand.append(Card(Number.Five, Suit.Diamond))

        hand.hand = tuple(new_hand)
        self.assertFalse(self.classifier.is_full_house(hand))

    def test_hand_does_not_contain_dupes(self):
        hand = FULL_HOUSE.copy()
        new_hand = list(hand.hand)
        new_hand[0] = new_hand[1]

        hand.hand = tuple(new_hand)
        self.assertFalse(self.classifier.is_full_house(hand))

    def test_hand_is_not_full_house(self):
        self.assertFalse(self.classifier.is_full_house(STRAIGHT_FLUSH))

    def test_bomb_is_not_full_house(self):
        self.assertFalse(self.classifier.is_full_house(BOMB))

    def test_hand_is_full_house(self):
        self.assertTrue(self.classifier.is_full_house(FULL_HOUSE))

    def test_range_valid(self):
        hand_level = self.classifier.calculate_value(FULL_HOUSE)

        self.assertLessEqual(combos.ComboLevels.FullHouse.value, hand_level)
        self.assertLessEqual(hand_level, combos.ComboLevels.FullHouse.value + combos.ComboLevels.LevelDelta.value)


class TestBomb(unittest.TestCase):
    def setUp(self):
        self.classifier = combos.ComboClassifier()

    def test_hand_is_right_length(self):
        hand = BOMB.copy()
        new_hand = list(hand.hand)
        new_hand.append(Card(Number.Five, Suit.Diamond))

        hand.hand = tuple(new_hand)
        self.assertFalse(self.classifier.is_bomb(hand))

    def test_hand_does_not_contain_dupes(self):
        hand = BOMB.copy()
        new_hand = list(hand.hand)
        new_hand[4] = new_hand[1]

        hand.hand = tuple(new_hand)
        self.assertFalse(self.classifier.is_bomb(hand))

    def test_hand_is_not_bomb(self):
        self.assertFalse(self.classifier.is_bomb(STRAIGHT_FLUSH))

    def test_fullhouse_is_not_bomb(self):
        self.assertFalse(self.classifier.is_bomb(FULL_HOUSE))

    def test_hand_is_bomb(self):
        self.assertTrue(self.classifier.is_bomb(BOMB))

    def test_range_valid(self):
        hand_level = self.classifier.calculate_value(BOMB)

        self.assertLessEqual(combos.ComboLevels.Bomb.value, hand_level)
        self.assertLessEqual(hand_level, combos.ComboLevels.Bomb.value + combos.ComboLevels.LevelDelta.value)


if __name__ == '__main__':
    unittest.main()

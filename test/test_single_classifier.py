
import unittest

import singles

from test.doubles.constant_hands import *


class TestSingles(unittest.TestCase):
    def setUp(self):
        self.classifier = singles.SingleClassifier()

    def test_invalid_lengths_fail(self):
        self.assertFalse(self.classifier.hand_is_correct_len_and_type(FULL_HOUSE))
        self.assertGreater(0, self.classifier.calculate_value(FULL_HOUSE))

    def test_values_calculated_correctly(self):
        smaller = self.classifier.calculate_value(EIGHT_DIAMOND)
        larger = self.classifier.calculate_value(EIGHT_SPADE)
        different_number = self.classifier.calculate_value(KING_SPADE)

        self.assertGreater(larger, smaller)
        self.assertGreater(different_number, larger)


if __name__ == '__main__':
    unittest.main()

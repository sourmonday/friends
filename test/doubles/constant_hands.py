
from cards import Card, Number, Suit
from hands import Hand


STRAIGHT = Hand((Card(Number.Four, Suit.Spade),
                 Card(Number.Five, Suit.Diamond),
                 Card(Number.Six, Suit.Diamond),
                 Card(Number.Seven, Suit.Club),
                 Card(Number.Eight, Suit.Diamond),
                 ))

BIGGER_STRAIGHT = Hand((Card(Number.Four, Suit.Diamond),
                        Card(Number.Five, Suit.Club),
                        Card(Number.Six, Suit.Club),
                        Card(Number.Seven, Suit.Spade),
                        Card(Number.Eight, Suit.Club),
                        ))

BIGGEST_STRAIGHT = Hand((Card(Number.Ten, Suit.Spade),
                         Card(Number.Jack, Suit.Diamond),
                         Card(Number.Queen, Suit.Diamond),
                         Card(Number.King, Suit.Club),
                         Card(Number.Ace, Suit.Diamond),
                         ))

UNORDERED_STRAIGHT = Hand((Card(Number.Four, Suit.Spade),
                           Card(Number.Six, Suit.Diamond),
                           Card(Number.Seven, Suit.Club),
                           Card(Number.Five, Suit.Diamond),
                           Card(Number.Eight, Suit.Diamond),
                           ))

INVALID_WRAPPED_STRAIGHT = Hand((Card(Number.Three, Suit.Spade),
                                 Card(Number.Four, Suit.Spade),
                                 Card(Number.Five, Suit.Spade),
                                 Card(Number.Six, Suit.Spade),
                                 Card(Number.Seven, Suit.Spade),
                                 ))

SPADE_FLUSH = Hand((Card(Number.Three, Suit.Spade),
                    Card(Number.Four, Suit.Spade),
                    Card(Number.Five, Suit.Spade),
                    Card(Number.Six, Suit.Spade),
                    Card(Number.Eight, Suit.Spade),
                    ))

LITTLE_SPADE_FLUSH = Hand((Card(Number.Ace, Suit.Spade),
                           Card(Number.Nine, Suit.Spade),
                           Card(Number.Ten, Suit.Spade),
                           Card(Number.Queen, Suit.Spade),
                           Card(Number.King, Suit.Spade),
                           ))

HEART_FLUSH = Hand((Card(Number.Three, Suit.Heart),
                    Card(Number.Four, Suit.Heart),
                    Card(Number.Five, Suit.Heart),
                    Card(Number.Eight, Suit.Heart),
                    Card(Number.King, Suit.Heart),
                    ))

FULL_HOUSE = Hand((Card(Number.Five, Suit.Spade),
                   Card(Number.Five, Suit.Heart),
                   Card(Number.Five, Suit.Club),
                   Card(Number.Four, Suit.Diamond),
                   Card(Number.Four, Suit.Heart),
                   ))

BIGGER_FULL_HOUSE = Hand((Card(Number.Seven, Suit.Spade),
                          Card(Number.Seven, Suit.Club),
                          Card(Number.Seven, Suit.Heart),
                          Card(Number.Eight, Suit.Club),
                          Card(Number.Eight, Suit.Diamond),
                          ))

BIGGER_BOMB = Hand((Card(Number.Seven, Suit.Spade),
                    Card(Number.Seven, Suit.Club),
                    Card(Number.Seven, Suit.Heart),
                    Card(Number.Seven, Suit.Diamond),
                    Card(Number.Eight, Suit.Diamond),
                    ))

BOMB = Hand((Card(Number.Five, Suit.Spade),
             Card(Number.Five, Suit.Heart),
             Card(Number.Five, Suit.Club),
             Card(Number.Five, Suit.Diamond),
             Card(Number.Four, Suit.Heart),
             ))

STRAIGHT_FLUSH = Hand((Card(Number.Four, Suit.Spade),
                       Card(Number.Seven, Suit.Spade),
                       Card(Number.Five, Suit.Spade),
                       Card(Number.Six, Suit.Spade),
                       Card(Number.Eight, Suit.Spade),
                       ))


# Quad Doubles

NOT_QUAD = Hand((Card(Number.Four, Suit.Spade),
                 Card(Number.Five, Suit.Spade),
                 Card(Number.Five, Suit.Diamond),
                 Card(Number.Five, Suit.Club),
                 ))

SMALLER_QUAD = Hand((Card(Number.Five, Suit.Spade),
                     Card(Number.Five, Suit.Heart),
                     Card(Number.Five, Suit.Club),
                     Card(Number.Five, Suit.Diamond),
                     ))

LARGER_QUAD = Hand((Card(Number.Seven, Suit.Spade),
                    Card(Number.Seven, Suit.Heart),
                    Card(Number.Seven, Suit.Club),
                    Card(Number.Seven, Suit.Diamond),
                    ))

# Triple Doubles

NOT_TRIPLE = Hand((Card(Number.Four, Suit.Spade),
                   Card(Number.Four, Suit.Diamond),
                   Card(Number.Six, Suit.Spade),
                   ))

SMALLER_TRIPLE = Hand((Card(Number.Five, Suit.Spade),
                       Card(Number.Five, Suit.Club),
                       Card(Number.Five, Suit.Diamond),
                       ))

LARGER_TRIPLE = Hand((Card(Number.Seven, Suit.Spade),
                      Card(Number.Seven, Suit.Club),
                      Card(Number.Seven, Suit.Diamond),
                      ))

# Pair Doubles

NOT_PAIR = Hand((Card(Number.Four, Suit.Spade),
                   Card(Number.Six, Suit.Spade),
                   ))

PAIR = Hand((Card(Number.Five, Suit.Heart),
                       Card(Number.Five, Suit.Diamond),
                       ))

SAME_NUMBER_PAIR = Hand((Card(Number.Five, Suit.Spade),
                      Card(Number.Five, Suit.Diamond),
                      ))

BIGGER_PAIR = Hand((Card(Number.Seven, Suit.Diamond),
                    Card(Number.Seven, Suit.Spade),
                    ))

# Single Doubles

EIGHT_SPADE = Hand((Card(Number.Eight, Suit.Spade), ))
EIGHT_DIAMOND = Hand((Card(Number.Eight, Suit.Diamond), ))
KING_SPADE = Hand((Card(Number.King, Suit.Spade),))
KING_DIAMOND = Hand((Card(Number.King, Suit.Diamond), ))

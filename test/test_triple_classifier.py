
import unittest

import triples

from test.doubles.constant_hands import *


class TestTriples(unittest.TestCase):
    def setUp(self):
        self.classifier = triples.TripleClassifier()

    def test_invalid_lengths_fail(self):
        self.assertFalse(self.classifier.hand_is_three(FULL_HOUSE))
        self.assertGreater(0, self.classifier.calculate_value(FULL_HOUSE))

    def test_not_a_triple_fail(self):
        self.assertFalse(self.classifier.hand_is_triple(NOT_TRIPLE))
        self.assertGreater(0, self.classifier.calculate_value(NOT_TRIPLE))

    def test_values_calculated_correctly(self):
        smaller = self.classifier.calculate_value(SMALLER_TRIPLE)
        larger = self.classifier.calculate_value(LARGER_TRIPLE)

        self.assertGreater(larger, smaller)

    def test_dupe_values_fails(self):
        hand = SMALLER_TRIPLE.copy()
        list_hand = list(hand.hand)

        list_hand[0] = list_hand[1]
        hand.hand = tuple(list_hand)

        self.assertGreater(0, self.classifier.calculate_value(hand))


if __name__ == '__main__':
    unittest.main()

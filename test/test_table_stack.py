
import unittest

import hands
import table

from test.doubles.constant_hands import *


class TestTableStack(unittest.TestCase):
    def setUp(self):
        self.stack = table.TableStack()
        self.hand_tup = ('fake card 1', )

    def test_push_tablestack_errors(self):
        with self.assertRaises(NotImplementedError):
            self.stack.push(hands.Hand(self.hand_tup))

    def test_peek(self):
        self.stack.insert(0, self.hand_tup)
        self.assertIs(self.hand_tup, self.stack.peek())


class TestComboStack(unittest.TestCase):
    def setUp(self):
        self.stack = table.ComboStack()

    def test_handtype_raises_error(self):
        pair = hands.Hand(('fake card 1', 'fake card 2'))

        with self.assertRaises(TypeError):
            self.stack.push(pair)

    def test_combo_works(self):
        self.stack.push(SPADE_FLUSH)
        self.assertEqual(len(self.stack), 1)

    def test_flush_after_straight_works(self):
        self.stack.push(STRAIGHT)
        self.stack.push(SPADE_FLUSH)

        self.assertEqual(len(self.stack), 2)

    def test_straight_after_flush_fails(self):
        self.stack.push(SPADE_FLUSH)

        with self.assertRaises(ValueError):
            self.stack.push(STRAIGHT)

    def test_straight_after_flush_reset_works(self):
        self.stack.push(SPADE_FLUSH)

        self.stack.reset_stack_level()
        self.stack.push(STRAIGHT)

        self.assertEqual(len(self.stack), 2)

    def test_valid_flush_suit_works(self):
        self.stack.push(HEART_FLUSH)
        self.stack.push(SPADE_FLUSH)

        self.assertEqual(len(self.stack), 2)

    def test_invalid_flush_suit_fails(self):
        self.stack.push(SPADE_FLUSH)

        with self.assertRaises(ValueError):
            self.stack.push(HEART_FLUSH)

    def test_valid_same_suit_flush_works(self):
        self.stack.push(LITTLE_SPADE_FLUSH)
        self.stack.push(SPADE_FLUSH)

        self.assertEqual(len(self.stack), 2)

    def test_invalid_same_suit_flush_fails(self):
        self.stack.push(SPADE_FLUSH)

        with self.assertRaises(ValueError):
            self.stack.push(LITTLE_SPADE_FLUSH)

    def test_valid_straight_order_works(self):
        self.stack.push(STRAIGHT)
        self.stack.push(BIGGER_STRAIGHT)
        self.stack.push(BIGGEST_STRAIGHT)

        self.assertEqual(len(self.stack), 3)

    def test_clearly_bigger_straight_invalid_order_fails(self):
        self.stack.push(BIGGEST_STRAIGHT)

        with self.assertRaises(ValueError):
            self.stack.push(STRAIGHT)

    def test_slightly_bigger_straight_invalid_order_fails(self):
        self.stack.push(BIGGER_STRAIGHT)

        with self.assertRaises(ValueError):
            self.stack.push(STRAIGHT)

    def test_valid_full_house_order(self):
        self.stack.push(FULL_HOUSE)
        self.stack.push(BIGGER_FULL_HOUSE)

        self.assertEqual(len(self.stack), 2)

    def test_invalid_full_house_order(self):
        self.stack.push(BIGGER_FULL_HOUSE)

        with self.assertRaises(ValueError):
            self.stack.push(FULL_HOUSE)

    def test_valid_bomb_order(self):
        self.stack.push(BOMB)
        self.stack.push(BIGGER_BOMB)

        self.assertEqual(len(self.stack), 2)

    def test_invalid_bomb_order(self):
        self.stack.push(BIGGER_BOMB)

        with self.assertRaises(ValueError):
            self.stack.push(BOMB)


class TestQuadStack(unittest.TestCase):
    def setUp(self):
        self.stack = table.QuadStack()

    def test_non_quad_raises_error(self):
        with self.assertRaises(TypeError):
            self.stack.push(FULL_HOUSE)

    def test_valid_order_works(self):
        self.stack.push(SMALLER_QUAD)
        self.stack.push(LARGER_QUAD)

        self.assertEqual(len(self.stack), 2)

    def test_reset_works(self):
        self.stack.push(LARGER_QUAD)

        self.stack.reset_stack_level()
        self.stack.push(SMALLER_QUAD)

        self.assertEqual(len(self.stack), 2)

    def test_invalid_order_raises_error(self):
        self.stack.push(LARGER_QUAD)

        with self.assertRaises(ValueError):
            self.stack.push(SMALLER_QUAD)


class TestTripleStack(unittest.TestCase):
    def setUp(self):
        self.stack = table.TripleStack()

    def test_non_triple_raises_error(self):
        with self.assertRaises(TypeError):
            self.stack.push(FULL_HOUSE)

    def test_valid_order_works(self):
        self.stack.push(SMALLER_TRIPLE)
        self.stack.push(LARGER_TRIPLE)

        self.assertEqual(len(self.stack), 2)

    def test_reset_works(self):
        self.stack.push(LARGER_TRIPLE)

        self.stack.reset_stack_level()
        self.stack.push(SMALLER_TRIPLE)

        self.assertEqual(len(self.stack), 2)

    def test_invalid_order_raises_error(self):
        self.stack.push(LARGER_TRIPLE)

        with self.assertRaises(ValueError):
            self.stack.push(SMALLER_TRIPLE)


class TestDoubleStack(unittest.TestCase):
    def setUp(self):
        self.stack = table.DoubleStack()

    def test_non_single_raises_error(self):
        with self.assertRaises(TypeError):
            self.stack.push(FULL_HOUSE)

    def test_valid_order_works(self):
        self.stack.push(PAIR)
        self.stack.push(BIGGER_PAIR)

        self.assertEqual(len(self.stack), 2)

    def test_reset_works(self):
        self.stack.push(BIGGER_PAIR)

        self.stack.reset_stack_level()
        self.stack.push(PAIR)

        self.assertEqual(len(self.stack), 2)

    def test_invalid_order_raises_error(self):
        self.stack.push(SAME_NUMBER_PAIR)

        with self.assertRaises(ValueError):
            self.stack.push(PAIR)


class TestSingleStack(unittest.TestCase):
    def setUp(self):
        self.stack = table.SingleStack()

    def test_non_single_raises_error(self):
        with self.assertRaises(TypeError):
            self.stack.push(FULL_HOUSE)

    def test_valid_order_works(self):
        self.stack.push(EIGHT_DIAMOND)
        self.stack.push(EIGHT_SPADE)
        self.stack.push(KING_SPADE)

        self.assertEqual(len(self.stack), 3)

    def test_invalid_order_raises_error(self):
        self.stack.push(EIGHT_SPADE)

        with self.assertRaises(ValueError):
            self.stack.push(EIGHT_DIAMOND)

    def test_suit_matching_if_not_number(self):
        self.stack.push(EIGHT_DIAMOND)

        with self.assertRaises(ValueError):
            self.stack.push(KING_SPADE)

    def test_reset_works(self):
        self.stack.push(EIGHT_SPADE)
        self.stack.reset_stack_level()
        self.stack.push(EIGHT_DIAMOND)

    def test_reset_suit_matching_works(self):
        self.stack.push(EIGHT_DIAMOND)
        self.stack.reset_stack_level()
        self.stack.push(KING_SPADE)


if __name__ == '__main__':
    unittest.main()

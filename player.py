
from cards import Card


class PlayerController:
    def __init__(self):
        self.hand = list()

    def add_card(self, card: Card) -> None:
        self.hand.append(card)

    def play_card(self, index) -> Card:
        return self.hand.pop(index)

    def add_hand(self, iterable) -> None:
        for i in iterable:
            assert isinstance(i, Card)
            self.add_card(i)

    def play_hand(self, indices: tuple) -> tuple:
        pass

    def cards_in_hand(self) -> int:
        return len(self.hand)


class FriendsPlayer(PlayerController):
    def __init__(self):
        super().__init__()
        self.friend = None

    def set_friend(self, other: 'FriendsPlayer'):
        self.friend = other


if __name__ == '__main__':
    pass

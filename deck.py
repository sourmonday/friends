
import random

from cards import Card, Number, Suit


class Deck:
    def __init__(self):
        self.stack = list()

    def generate_deck(self) -> None:
        for s in Suit:
            for n in Number:
                self.stack.append(Card(n, s))

    def shuffle(self) -> None:
        random.shuffle(self.stack)

    def cut(self) -> Card:
        return self.stack[random.randint(0, len(self.stack) - 1)]

    def deal(self) -> Card:
        return self.stack.pop()

    def __repr__(self):
        return "Deck of %d cards" % len(self.stack)


if __name__ == '__main__':
    pass

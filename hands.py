
from copy import copy
from enum import Enum


class HandType(Enum):
    Singles = 1
    Doubles = 2
    Triples = 3
    Quads = 4
    Combos = 5


rules_dispatch = {HandType.Singles: None,
                  HandType.Doubles: None,
                  HandType.Triples: None,
                  HandType.Quads: None,
                  HandType.Combos: None,
                  }


class Hand:
    def __init__(self, hand: tuple):
        self.hand = hand
        self.type = HandType(len(hand))

    def __cmp__(self, other):
        pass

    def __len__(self):
        return len(self.hand)

    def copy(self):
        return copy(self)


if __name__ == '__main__':
    pass

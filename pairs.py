
from cards import Card
from hands import Hand, HandType


class DoubleClassifier:
    def calculate_value(self, hand: Hand) -> int:
        if not self.hand_is_correct_len_and_type(hand) or not self.valid_hand_elements(hand) or \
                self.duplicates_exist(hand):
            return -1000

        hand_list = list(hand.hand)
        hand_list.sort()

        larger_card = hand_list[1]
        assert isinstance(larger_card, Card)
        return larger_card.number.value * 10 + larger_card.suit.value

    @staticmethod
    def hand_is_correct_len_and_type(hand: Hand) -> bool:
        return len(hand) is 2 and hand.type is HandType.Doubles

    @staticmethod
    def valid_hand_elements(hand: Hand) -> bool:
        first_card = hand.hand[0]
        assert isinstance(first_card, Card)

        for _, c, in enumerate(hand.hand):
            assert isinstance(c, Card)
            if first_card.number.value != c.number.value:
                return False
        return True

    @staticmethod
    def duplicates_exist(hand: Hand) -> bool:
        return len(hand) != len(set(hand.hand))

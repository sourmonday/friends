
from collections import Counter
from enum import Enum

from cards import Card
from hands import Hand, HandType


class ComboLevels(Enum):
    Straight = 1000
    Flush = 2000
    FullHouse = 3000
    Bomb = 4000
    StraightFlush = 5000
    LevelDelta = 999


class ComboClassifier:
    def is_straight(self, hand: Hand) -> bool:
        if not self.combo_length_check(hand):
            return False

        hand = list(hand.hand)  # TODO: ComboClassifer has knowledge of implementation of Hand
        hand.sort()
        first_card = hand[0]
        assert isinstance(first_card, Card)

        start_value = first_card.number.value
        for index, card in enumerate(hand):
            assert isinstance(card, Card)
            if card.number.value - index != start_value:
                return False
        return True

    def is_flush(self, hand: Hand) -> bool:
        if not self.combo_length_check(hand) or self.duplicates_exist(hand):
            return False

        first_card = hand.hand[0]
        assert isinstance(first_card, Card)

        matching_suit = first_card.suit
        for _, card in enumerate(hand.hand):
            assert isinstance(card, Card)
            if card.suit is not matching_suit:
                return False
        return True

    def is_straight_flush(self, hand: Hand) -> bool:
        return self.is_straight(hand) and self.is_flush(hand)

    def is_full_house(self, hand: Hand) -> bool:
        if not self.combo_length_check(hand) or self.duplicates_exist(hand):
            return False

        c = Counter(map(lambda card: card.number, hand.hand))
        if not self.two_elements_only(c):
            return False

        most, least = self.two_elements_count(c)
        return most == 3 and least == 2

    def is_bomb(self, hand: Hand) -> bool:
        if not self.combo_length_check(hand) or self.duplicates_exist(hand):
            return False

        c = Counter(map(lambda card: card.number, hand.hand))
        if not self.two_elements_only(c):
            return False

        most, least = self.two_elements_count(c)
        return most == 4 and least == 1

    @staticmethod
    def combo_length_check(hand: Hand) -> bool:
        return hand.type == HandType.Combos and len(hand.hand) is HandType.Combos.value

    @staticmethod
    def two_elements_only(c: Counter)-> bool:
        return len(c) == 2

    @staticmethod
    def duplicates_exist(hand: Hand) -> bool:
        return len(hand) != len(set(hand.hand))

    @staticmethod
    def largest_card(hand: Hand) -> Card:
        hand = list(hand.hand)
        hand.sort()

        return hand[-1]

    @classmethod
    def two_elements_count(cls, c: Counter) -> tuple:
        assert cls.two_elements_only(c)
        return c.most_common(1).pop(0)[1], c.most_common(2).pop(1)[1]

    # TODO: ComboClassifier has knowledge of implementation of Suit
    # TODO: ComboClassifier has knowledge of implementation of Number
    def calculate_value(self, hand: Hand) -> int:
        largest_card = self.largest_card(hand)

        if self.is_straight_flush(hand):
            return ComboLevels.StraightFlush.value

        if self.is_flush(hand):
            flush_value_modified = largest_card.suit.value * 100
            return ComboLevels.Flush.value + flush_value_modified + largest_card.number.value
        if self.is_straight(hand):
            number_value_modified = largest_card.number.value * 10
            return ComboLevels.Straight.value + largest_card.suit.value + number_value_modified

        c = Counter(map(lambda card: card.number, hand.hand))
        most_card_number = c.most_common(1).pop(0)[0]
        assert isinstance(most_card_number, Enum)

        if self.is_full_house(hand):
            return ComboLevels.FullHouse.value + most_card_number.value
        if self.is_bomb(hand):
            return ComboLevels.Bomb.value + most_card_number.value

        return -1000


if __name__ == '__main__':
    pass


from enum import Enum

class Suit(Enum):
    Spade = 4
    Heart = 3
    Club = 2
    Diamond = 1


class Number(Enum):
    Three = 16
    Two = 15
    Ace = 14
    King = 13
    Queen = 12
    Jack = 11
    Ten = 10
    Nine = 9
    Eight = 8
    Seven = 7
    Six = 6
    Five = 5
    Four = 4


suit_str_lookup = {Suit.Spade: "Spades", Suit.Heart: "Hearts", Suit.Club: "Clubs", Suit.Diamond: "Diamonds"}
number_str_lookup = {Number.Three: "Three", Number.Two: "Two", Number.Ace: "Ace", Number.King: "King",
                     Number.Queen: "Queen", Number.Jack: "Jack", Number.Ten: "Ten", Number.Nine: "Nine",
                     Number.Eight: "Eight", Number.Seven: "Seven", Number.Six: "Six", Number.Five: "Five",
                     Number.Four: "Four"}


class Card:
    def __init__(self, number: Number, suit: Suit):
        self.number = number
        self.suit = suit

    def __lt__(self, other: 'Card'):
        if self.number.value == other.number.value:
            return self.suit.value < other.suit.value
        return self.number.value < other.number.value

    def __hash__(self):
        return hash(self.number) + hash(self.suit)

    def __eq__(self, other):
        return self.__class__ == other.__class__ and self.number.value == other.number.value \
               and self.suit.value == other.suit.value

    def __repr__(self):
        return "%s of %s" % (number_str_lookup[self.number], suit_str_lookup[self.suit])


if __name__ == '__main__':
    pass
